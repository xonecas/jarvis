#!/usr/bin/env python

"""
Jarvis.py

Jarvis helps us maintain our dev environment, and easily use it on any
machine we might need it on

`$ jarvis vim read|write`

Interface to handle vim configurations, only two actions supported:
    read -- Read from the current environment the vim setup, including config
            and any plugin installed (pathogen might be a special case)

    write - import the last read setup, this requires the repo to be up to date
            and we will use git to version this for us.


@TODO:  bash configuration and aliases, should also have read and write cmds.
@TODO:  ensure the right environments tools are installed
        (nvm, rbenv, and virtualenv)
"""

import argparse
import sys


def main():
    """ command disambiguiation """
    parser = argparse.ArgumentParser(description="Disambiguate commands")
    # It would be nice to parse `jarvis vim read`
    parser.add_argument('vim', action='store_true')
    args = parser.parse_args()

    print args
    print args.vim

    return 0


if __name__ == "__main__":
    sys.exit(main())
